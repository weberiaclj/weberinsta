(defproject wia "1.0.0"
  :description "Implementation of Weberia Instituional Agent"
  :url "http://github.com/weberia/wia"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]])
